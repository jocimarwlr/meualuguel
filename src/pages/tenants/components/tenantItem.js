import { View, Text, StyleSheet, Pressable, Linking } from 'react-native';
import React from 'react';
import { FontAwesome } from '@expo/vector-icons'; // Importe o ícone do FontAwesome

const TenantItem = ({ tenant }) => {
  const handleWhatsApp = () => {
    if (tenant.phoneNumber) {
      const whatsappMessage = 'Olá, estou entrando em contato sobre o aluguel.';
      const whatsappLink = `whatsapp://send?phone=${tenant.phoneNumber}&text=${whatsappMessage}`;
      Linking.openURL(whatsappLink);
    } else {
      console.log('Número de telefone não disponível');
    }
  };

  const handleCall = () => {
    if (tenant.phoneNumber) {
      const phoneNumber = `tel:${tenant.phoneNumber}`;
      Linking.openURL(phoneNumber);
    } else {
      console.log('Número de telefone não disponível');
    }
  };

  return (
    <View style={styles.container}>
      <Pressable onPress={handleWhatsApp} style={styles.iconContainer}>
        {/* Aqui adicionamos o ícone do WhatsApp */}
        <FontAwesome name="whatsapp" size={24} color="green" style={styles.icon} />
      </Pressable>
      <View style={styles.info}>
        <Pressable onPress={handleWhatsApp}>
          <Text style={[styles.text, styles.name]}>{tenant.name}</Text>
        </Pressable>
        <Text style={[styles.text, styles.houseNumber]}>{tenant.houseNumber}</Text>
      </View>
      <Pressable onPress={handleCall} style={styles.phoneNumberContainer}>
        <Text style={[styles.text, styles.phoneNumber]}>{tenant.phoneNumber}</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row', // Altera o layout para linha
    backgroundColor: '#0e0e0e',
    padding: 14,
    width: '100%',
    marginBottom: 14,
    borderRadius: 8,
    alignItems: 'center', // Alinha os itens verticalmente
  },
  text: {
    color: '#FFF',
  },
  name: {
    fontWeight: 'bold', // Adiciona negrito para destacar o nome
    marginRight: 8, // Adiciona espaçamento à direita do nome
  },
  houseNumber: {
   
    marginLeft: 8, // Adiciona espaçamento à esquerda do número da casa
  },
  phoneNumber: {
    textDecorationLine: 'underline', // Adiciona sublinhado para indicar que é clicável
  },
  iconContainer: {
    marginRight: 8, // Adiciona espaçamento à direita do ícone
  },
  icon: {
    marginLeft: 'auto', // Empurra o ícone para a extremidade esquerda
  },
  info: {
    flexDirection: 'row', // Altera o layout para linha
    alignItems: 'center', // Alinha os itens verticalmente
    flex: 1, // Permite que o conteúdo na view ocupe todo o espaço possível
  },
  phoneNumberContainer: {
    marginLeft: 'auto', // Empurra o número de telefone para a extremidade direita
  },
});

export default TenantItem;
