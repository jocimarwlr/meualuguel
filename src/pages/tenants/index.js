// No arquivo onde você define o componente Tenants

import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { tenantGetApi } from "../../tenantGetApi"; // Importando a função do arquivo tenantGetApi.js
import { useIsFocused } from "@react-navigation/native";
import { SafeAreaView } from "react-native-safe-area-context";
import TenantItem from "./components/tenantItem";

export function Tenants() {
  const [tenantsData, setTenantsData] = useState([]);
  const focused = useIsFocused();
  const [refreshing, setRefreshing] = useState(false); // State to manage refreshing state

  useEffect(() => {
    getTenants();
  }, [focused]);


  const getTenants = async () => {
    console.log("Process getTenats!!")
    try {
      const data = await tenantGetApi(); // Chama a função para obter os dados da API
      setTenantsData(data);
    } catch (error) {
      // Tratar erros, se necessário
      console.error("Error fetching data:", error);
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.header}>
        <Text style={styles.title}>Moradores</Text>
      </View>
      <View style={styles.content}>
        <FlatList
          style={{ flex: 1, paddingTop: 14 }}
          data={tenantsData}
          keyExtractor={(item) => String(item.id)}
          renderItem={({ item }) => <TenantItem tenant={item} />}
          refreshing={refreshing}
          onRefresh={() => {
            setRefreshing(true);
            getTenants().then(() => setRefreshing(false));
          }}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  // Seus estilos aqui

  header: {
    backgroundColor: "#392de9",
    paddingTop: 58,
    paddingBottom: 14,
    paddingLeft: 14,
    paddingRight: 14,
  },

  title: {
    fontSize: 24,
    color: "#FFF",
    fontWeight: "bold",
  },
  content: {
    flex: 1,
    paddingLeft: 14,
    paddingRight: 14,
  },
});
