import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Modal } from 'react-native';
import { useIsFocused } from "@react-navigation/native";
import Icon from 'react-native-vector-icons/FontAwesome';
import * as Notifications from 'expo-notifications';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NewPaymentModal from '../../components/newpaymentmodal';
import PaymentsList from '../../components/lastpayments';

const Payments = () => {
    const focused = useIsFocused();
    const [tenants, setTenants] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [shouldUpdateList, setShouldUpdateList] = useState(false);
    const [notifiedPayments, setNotifiedPayments] = useState([]);

    useEffect(() => {
        if (focused) {
            console.log("Entrou na análise");
            checkPayments();
        }
    }, [focused]);

    useEffect(() => {
        registerForPushNotificationsAsync();
        resetNotifiedPaymentsIfNewMonth();
        loadNotifiedPayments();
    }, []);

    useEffect(() => {
        Notifications.setNotificationHandler({
            handleNotification: async () => ({
                shouldShowAlert: true,
                shouldPlaySound: true,
                shouldSetBadge: true,
            }),
        });
    }, []);

    useEffect(() => {
        if (shouldUpdateList) {
            console.log("Updating Payment List...");
            // Aqui você pode realizar qualquer ação necessária para atualizar a lista de pagamentos
            // Por exemplo, recarregar dados do servidor ou atualizar o estado
        }
    }, [shouldUpdateList]);

    const loadNotifiedPayments = async () => {
        try {
            const savedData = await AsyncStorage.getItem('notifiedPayments');
            if (savedData !== null) {
                const parsedData = JSON.parse(savedData);
                setNotifiedPayments(parsedData.payments || []);
            }
        } catch (error) {
            console.error('Erro ao carregar pagamentos notificados:', error);
        }
    };

    const saveNotifiedPayments = async (payments) => {
        try {
            const dataToSave = {
                date: new Date().toISOString(),
                payments,
            };
            await AsyncStorage.setItem('notifiedPayments', JSON.stringify(dataToSave));
        } catch (error) {
            console.error('Erro ao salvar pagamentos notificados:', error);
        }
    };

    const resetNotifiedPaymentsIfNewMonth = async () => {
        try {
            const savedData = await AsyncStorage.getItem('notifiedPayments');
            if (savedData !== null) {
                const parsedData = JSON.parse(savedData);
                const lastSavedDate = new Date(parsedData.date);
                const currentDate = new Date();

                if (lastSavedDate.getMonth() !== currentDate.getMonth() || lastSavedDate.getFullYear() !== currentDate.getFullYear()) {
                    await AsyncStorage.removeItem('notifiedPayments');
                    setNotifiedPayments([]);
                }
            }
        } catch (error) {
            console.error('Erro ao redefinir pagamentos notificados:', error);
        }
    };

    const registerForPushNotificationsAsync = async () => {
        console.log("entrou em registerForPushNotificationsAsync")
        const { status: existingStatus } = await Notifications.getPermissionsAsync();
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            const { status } = await Notifications.requestPermissionsAsync();
            finalStatus = status;
        }
        if (finalStatus !== 'granted') {
            return;
        }
        const token = (await Notifications.getExpoPushTokenAsync()).data;
        console.log("Expo Push Token:", token);
        // Envie este token para seu servidor para associá-lo ao usuário correspondente
    };

    const handleClose = () => {
        setModalVisible(false);
        setShouldUpdateList(prevState => !prevState);
    };

    const checkPayments = async () => {
        try {
            const response = await fetch('https://inovaestudios.com.br/meualuguel-api/tenants');
            const data = await response.json();
            setTenants(data);
            data.forEach(tenant => {
                if (isPaymentDue(tenant.invoiceDate) && !notifiedPayments.includes(tenant.id)) {
                    sendNotification(tenant.name);
                    const updatedPayments = [...notifiedPayments, tenant.id];
                    setNotifiedPayments(updatedPayments);
                    saveNotifiedPayments(updatedPayments);
                }
            });
        } catch (error) {
            console.error('Erro ao verificar pagamentos:', error);
        }
    };

    const isPaymentDue = (invoiceDate) => {
        if (!invoiceDate) return false;

        const parts = invoiceDate.split('-');
        const year = parseInt(parts[0]);
        const month = parseInt(parts[1]) - 1;
        const day = parseInt(parts[2]);

        const dueDate = new Date(Date.UTC(year, month, day));
        const today = new Date();

        const dueDay = dueDate.getUTCDate();
        const todayDay = today.getDate();
        const todayMonth = today.getMonth();
        const todayYear = today.getFullYear();

        // Verifica se a data da fatura é igual ao dia atual e não é do mês/ano corrente
        if (dueDay === todayDay && !(year === todayYear && month === todayMonth)) {
            // Se o mês atual for fevereiro e o dia da fatura for 29, 30 ou 31, verifique se é um ano bissexto
            if (todayMonth === 1 && (dueDay === 29 || dueDay === 30 || dueDay === 31)) {
                // Verifica se o ano é bissexto
                if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
                    return true; // Se for bissexto, a fatura é válida
                } else {
                    return false; // Se não for bissexto, a fatura é inválida
                }
            } else {
                return true; // Outros casos são válidos
            }
        }

        return false; // Se não for o mesmo dia ou mês/ano corrente, retorna false
    };

    const sendNotification = async (tenantName) => {
        const identifier = await Notifications.scheduleNotificationAsync({
            content: {
                title: 'Aluguel Vencendo',
                body: `O aluguel de ${tenantName} está vencendo hoje.`,
            },
            trigger: null,
        });
        console.log("Scheduled notification with id:", identifier);
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.title}>Valores Recebidos</Text>
                </View>
                <PaymentsList key={shouldUpdateList.toString()} />
                <TouchableOpacity style={styles.floatingButton} onPress={() => { setModalVisible(!modalVisible) }}>
                    <Icon name="plus" size={24} color="#FFF" />
                </TouchableOpacity>
                <Modal visible={modalVisible} animationType="fade" transparent={true} >
                    <NewPaymentModal onClose={handleClose} />
                </Modal>
            </View>
        </SafeAreaView>
    );
};

const circleSize = 48;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: "#392de9",
        paddingTop: 58,
        paddingBottom: 14,
        paddingLeft: 14,
        paddingRight: 14,
    },
    title: {
        fontSize: 24,
        color: '#FFF',
        fontWeight: 'bold'
    },
    floatingButton: {
        position: 'absolute',
        bottom: 60,
        right: 20,
        backgroundColor: '#392de9',
        borderRadius: circleSize / 2,
        width: circleSize,
        height: circleSize,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 8,
    },
});

export default Payments;
