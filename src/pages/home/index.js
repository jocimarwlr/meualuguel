import { View, Text, StyleSheet, Image, TouchableOpacity, Modal } from "react-native";
import React, { useState } from "react";
import ModalNovoMorador from "../../components/modal";

export function Home()  {
 
  const [modalVisible,setModalVisible]=useState(false);



  function handleRegister(){
    setModalVisible(true);
  }

  const handleCloseModal = () => {
    setModalVisible(false);
  };


  return (
    <View style={styles.container}>
      <Text style={styles.myText}>MeuAluguel</Text>
      <View style={styles.areaImage}>
        <Image source={require("../../assets/home.jpg")} style={styles.logo} />
      </View>
      
       <TouchableOpacity style={styles.button} onPress={handleRegister}>
          <Text style={styles.buttonText}>Novo Morador</Text>
       </TouchableOpacity>

       <Modal visible={modalVisible} animationType="fade" transparent={true}>
        <ModalNovoMorador onClose={handleCloseModal}/>
       </Modal>
      
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFF",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  myText: {
    fontSize: 34,
    fontWeight: "bold",
    marginBottom:50
  },
  logo: {},
  areaImage: {
   marginBottom:20
  },
  areaButton: {
    backgroundColor: "blue",
    width: "80%",
    height: 40,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
  },
  textButton: {
    color: "#FFF",
    fontSize: 20,
    fontWeight: "bold",
   
    
  },

  pressableButton: {
    width: "100%",
    height: "100%",
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center"
  },
  button:{
    backgroundColor:'#392de9',
    width:"80%",
    borderRadius:8,
    padding:10,
    alignItems:'center',
    justifyContent:'center'
  },

  buttonText:{
    color:'#FFF',
    fontWeight:'bold',
    fontSize:20
  }

});


