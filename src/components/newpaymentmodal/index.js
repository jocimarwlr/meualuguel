import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import React, { useState, useEffect } from "react";
import { Picker } from '@react-native-picker/picker';
import { TextInput } from 'react-native';

const NewPaymentModal = ({ onClose }) => {
  const [tenants, setTenants] = useState([]);
  const [selectedTenant, setSelectedTenant] = useState('');
  const [amountPaid, setAmountPaid] = useState('');


  const amountPaidFloat = parseFloat(amountPaid.replace(/[^\d,]/g, '').replace(',', '.'));

  useEffect(() => {
    fetchTenants();
  }, []);

  const handleValorAluguelChange = (text) => {
    let formatted = text.replace(/\D/g, '');
    formatted = (parseInt(formatted) / 100).toFixed(2).replace('.', ',');
    formatted = 'R$ ' + formatted.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    setAmountPaid(formatted);
  };

  const fetchTenants = async () => {
    try {
      const response = await fetch('https://inovaestudios.com.br/meualuguel-api/tenants');
      const data = await response.json();
      setTenants(data);
    } catch (error) {
      console.error('Error fetching tenants:', error);
    }
  };

  const handleSavePayment = async () => {
    try {
      const response = await fetch('https://inovaestudios.com.br/meualuguel-api/payments', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          amountPaid: amountPaidFloat,
          tenantId: selectedTenant
        })
      });
      if (response.ok) {
        console.log('Payment saved successfully');
        onClose(); // Fechar modal após o pagamento ser salvo
      } else {
        alert('Failed to save payment . httpStatus:'+ response.status)

        console.error('Failed to save payment . httpStatus:'+ response.status);
      }
    } catch (error) {
      console.error('Error saving payment:', error);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text>Selecione o Morador:</Text>
        <Picker
          selectedValue={selectedTenant}
          onValueChange={(itemValue) => setSelectedTenant(itemValue)}
        >
          {tenants.map(tenant => (
            <Picker.Item style={{fontSize:20}} key={tenant.id} label={`${tenant.name} - ${tenant.houseNumber}`} value={tenant.id} />
          ))}
        </Picker>
        <Text>Valor do Pagamento:</Text>
        <TextInput
          style={{fontSize:20}}
          value={amountPaid}
          onChangeText={handleValorAluguelChange}
          keyboardType="numeric"
          placeholder="Digite o valor"
        />
        <View style={styles.containerButton}>
          <TouchableOpacity style={styles.button} onPress={onClose}>
            <Text style={styles.buttonText}>Cancelar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={handleSavePayment}>
            <Text style={styles.buttonText}>Salvar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    flex: 1,
    backgroundColor: '#007bff',
    paddingVertical: 12,
    borderRadius: 4,
    alignItems: 'center',
    marginVertical: 12,
    marginHorizontal: 5,
  },
  content: {
    backgroundColor: "#CCC",
    width: "85%",
    paddingVertical: 24,
    paddingHorizontal: 16,
    borderRadius: 8,
  },
  container:{
    backgroundColor:'',
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  containerButton:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop:25
  },
  buttonText:{
    color: '#FFF',
    fontSize: 16,
  }
});

export default NewPaymentModal;
