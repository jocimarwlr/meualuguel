import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { format } from 'date-fns';
import { MaterialIcons } from '@expo/vector-icons';
import axios from 'axios';

const PaymentsList = () => {
  const [payments, setPayments] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);
  const [refreshing, setRefreshing] = useState(false); // State to manage refreshing state

  useEffect(() => {
    fetchPayments();
  }, []);

  const fetchPayments = async () => {

      try {
        const response = await axios.get("https://inovaestudios.com.br/meualuguel-api/payments");
        console.log("Response",response.data)
        setPayments(response.data);
        const total = response.data.reduce((acc, payment) => acc + payment.amountPaid, 0);
        setTotalAmount(total);
        
      } catch (error) {
        console.log("Error while find payments", error)
        
      }

    

  
  };

  const formatCurrency = (value) => {
    return new Intl.NumberFormat('pt-BR', {
      style: 'currency',
      currency: 'BRL'
    }).format(value);
  };

  const formatDate = (date) => {
    try {
      // Verifica se a data é uma string
      if (typeof date !== 'string') {
        throw new Error('Data não é uma string');
      }
      
      // Formata a data para o formato desejado
      const [year, month, day] = date.split('-');
      return `${day}/${month}/${year}`;
    } catch (error) {
      console.error('Erro ao formatar data:', error);
      return ''; // Retorna uma string vazia em caso de erro
    }
  };
  

  
  

  const handleDelete = (paymentId) => {
    fetch(`https://inovaestudios.com.br/meualuguel-api/payments/${paymentId}`, {
      method: 'DELETE'
    })
      .then(response => {
        if (response.ok) {
          Alert.alert('Pagamento excluído com sucesso');
          fetchPayments(); // Atualiza a lista após a exclusão
        } else {
          throw new Error('Erro ao excluir pagamento');
        }
      })
      .catch(error => {
        console.error('Erro ao excluir pagamento:', error);
        Alert.alert('Erro ao excluir pagamento');
      });
  };

  const renderItem = ({ item }) => (
    <View style={styles.card}>
      <View style={styles.iconContainer}>
        <TouchableOpacity onPress={() => handleDelete(item.id)}>
          <MaterialIcons name="delete" size={18} color="#888" style={styles.icon} />
        </TouchableOpacity>
      </View>
      <Text style={styles.text}>{`Valor: ${formatCurrency(item.amountPaid)}`}</Text>
      <Text style={styles.text}>{`Data: ${formatDate(item.paymentDate)}`}</Text>
      <Text style={styles.text}>{`Morador: ${item.nameAndHouseNumber}`}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={payments}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        refreshing={refreshing}
        onRefresh={() => {
          setRefreshing(true);
          fetchPayments().then(() => setRefreshing(false));
        }}
      />
      <Text style={styles.totalAmount}>{`Valores Recebidos no mês: ${formatCurrency(totalAmount)}`}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#f0f0f0',
  },
  card: {
    backgroundColor: '#fff',
    padding: 15,
    marginBottom: 10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    elevation: 5,
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: 10,
  },
  icon: {
    marginLeft: 10,
  },
  text: {
    fontSize: 16,
    marginBottom: 5,
  },
  totalAmount: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
  },
});

export default PaymentsList;
