import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Switch,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import { salvarNovoMorador } from '../../api';
import DateTimePicker from '@react-native-community/datetimepicker';

const ModalNovoMorador = ({ onClose }) => {
  const [nome, setNome] = useState('');
  const [cpf, setCpf] = useState('');
  const [telefone, setTelefone] = useState('');
  const [rg, setRg] = useState('');
  const [email, setEmail] = useState('');
  const [numeroQuarto, setNumeroQuarto] = useState('');
  const [valorAluguel, setValorAluguel] = useState('');
  const [dataEntrada, setDataEntrada] = useState(new Date());
  const [receberNotificacoes, setReceberNotificacoes] = useState(false);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [smsNotificacao, setSmsNotificacao] = useState(false);
  const [whatsappNotificacao, setWhatsappNotificacao] = useState(false);
  const [emailNotificacao, setEmailNotificacao] = useState(false);

  const validarCPF = (cpf) => {
    console.log("Valor cpf:", cpf)
    if(cpf===null || cpf===undefined || cpf===''){
      return true;
    }
    cpf = cpf.replace(/\D/g, '');
    if (cpf.length !== 11 || /^(.)\1+$/.test(cpf)) return false;

    let sum = 0;
    let rest;

    for (let i = 1; i <= 9; i++) sum += parseInt(cpf.substring(i - 1, i)) * (11 - i);
    rest = (sum * 10) % 11;

    if (rest === 10 || rest === 11) rest = 0;
    if (rest !== parseInt(cpf.substring(9, 10))) return false;

    sum = 0;
    for (let i = 1; i <= 10; i++) sum += parseInt(cpf.substring(i - 1, i)) * (12 - i);
    rest = (sum * 10) % 11;

    if (rest === 10 || rest === 11) rest = 0;
    if (rest !== parseInt(cpf.substring(10, 11))) return false;

    return true;
  };

  const valorAluguelFloat = parseFloat(valorAluguel.replace(/[^\d,]/g, '').replace(',', '.'));
  const handleSalvar = async () => {
    if (!validarCPF(cpf)) {
      alert('CPF inválido!');
      return;
    }
    
    console.log('Dados do novo morador:');
    console.log('Nome:', nome);
    console.log('CPF:', cpf);
    console.log('Telefone:', telefone);
    console.log('RG:', rg);
    console.log('Email:', email);
    console.log('Número do Quarto:', numeroQuarto);
    console.log('Valor do Aluguel:', valorAluguelFloat);
    console.log('Data de Entrada:', dataEntrada);
    console.log('Receber notificações:', receberNotificacoes);
    console.log('Notificações SMS:', smsNotificacao);
    console.log('Notificações WhatsApp:', whatsappNotificacao);
    console.log('Notificações Email:', emailNotificacao);



    try {
      await salvarNovoMorador({
        nome,
        cpf,
        telefone,
        rg,
        email,
        numeroQuarto,
        valorAluguel: valorAluguelFloat,
        dataEntrada,
        smsNotificacao,
        whatsappNotificacao,
        emailNotificacao,
      });
      alert('Novo morador salvo com sucesso!');
      onClose();
    } catch (error) {
      console.log("Erro ao salvar", error)
      alert('Erro ao salvar novo morador. Por favor, tente novamente.');
    }
  };

  const formatCPF = (text) => {
    let formatted = text.replace(/\D/g, '');
    formatted = formatted.slice(0, 11);
    formatted = formatted.replace(/(\d{3})(\d)/, '$1.$2');
    formatted = formatted.replace(/(\d{3})(\d)/, '$1.$2');
    formatted = formatted.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
    setCpf(formatted);
  };

  const formatTelefone = (text) => {
    let formatted = text.replace(/\D/g, '');
    formatted = formatted.slice(0, 11);
    formatted = formatted.replace(/^(\d{2})(\d)/g, '($1) $2');
    formatted = formatted.replace(/(\d{5})(\d)/, '$1-$2');
    setTelefone(formatted);
  };

  const handleValorAluguelChange = (text) => {
    let formatted = text.replace(/\D/g, '');
    formatted = (parseInt(formatted) / 100).toFixed(2).replace('.', ',');
    formatted = 'R$ ' + formatted.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    setValorAluguel(formatted);
  };

  const handleDataEntradaChange = (event, selectedDate) => {
    setShowDatePicker(Platform.OS === 'ios');
    if (selectedDate) setDataEntrada(selectedDate);
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      style={styles.container}
    >
      <ScrollView contentContainerStyle={styles.scrollView}>
        <View style={styles.content}>
          <Text style={styles.title}>Informe os dados do novo morador</Text>
          <TextInput
            style={styles.input}
            placeholder="Nome"
            value={nome}
            onChangeText={setNome}
          />
          <TextInput
            style={styles.input}
            placeholder="CPF"
            value={cpf}
            onChangeText={formatCPF}
            keyboardType="numeric"
          />
          <TextInput
            style={styles.input}
            placeholder="Telefone"
            value={telefone}
            onChangeText={formatTelefone}
            keyboardType="phone-pad"
          />
          <TextInput
            style={styles.input}
            placeholder="RG"
            value={rg}
            onChangeText={setRg}
          />
          <TextInput
            style={styles.input}
            placeholder="Email"
            value={email}
            onChangeText={setEmail}
            keyboardType="email-address"
          />
          <TextInput
            style={styles.input}
            placeholder="Número do Quarto"
            value={numeroQuarto}
            onChangeText={setNumeroQuarto}
            keyboardType="numeric"
          />
          <TextInput
            style={styles.input}
            placeholder="Valor do Aluguel"
            value={valorAluguel}
            onChangeText={handleValorAluguelChange}
            keyboardType="numeric"
          />
          <TouchableOpacity
            style={styles.input}
            onPress={() => setShowDatePicker(true)}
          >
            <Text>{`${dataEntrada.getDate().toString().padStart(2, '0')}/${(dataEntrada.getMonth() + 1).toString().padStart(2, '0')}/${dataEntrada.getFullYear()}`}</Text>
          </TouchableOpacity>
          {showDatePicker && (
            <DateTimePicker
              value={dataEntrada}
              mode="date"
              display="default"
              onChange={handleDataEntradaChange}
            />
          )}
          <View style={styles.switchContainer}>
            <Text style={styles.switchText}>Receber notificações:</Text>
            <Switch              trackColor={{ false: '#767577', true: '#81b0ff' }}
              thumbColor={receberNotificacoes ? '#f5dd4b' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={setReceberNotificacoes}
              value={receberNotificacoes}
            />
          </View>
          {receberNotificacoes && (
            <View>
              <View style={styles.switchContainer}>
                <Text style={styles.switchText}>SMS:</Text>
                <Switch
                  trackColor={{ false: '#767577', true: '#81b0ff' }}
                  thumbColor={smsNotificacao ? '#f5dd4b' : '#f4f3f4'}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={setSmsNotificacao}
                  value={smsNotificacao}
                />
              </View>
              <View style={styles.switchContainer}>
                <Text style={styles.switchText}>WhatsApp:</Text>
                <Switch
                  trackColor={{ false: '#767577', true: '#81b0ff' }}
                  thumbColor={whatsappNotificacao ? '#f5dd4b' : '#f4f3f4'}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={setWhatsappNotificacao}
                  value={whatsappNotificacao}
                />
              </View>
              <View style={styles.switchContainer}>
                <Text style={styles.switchText}>Email:</Text>
                <Switch
                  trackColor={{ false: '#767577', true: '#81b0ff' }}
                  thumbColor={emailNotificacao ? '#f5dd4b' : '#f4f3f4'}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={setEmailNotificacao}
                  value={emailNotificacao}
                />
              </View>
            </View>
          )}
          <View style={styles.buttonsContainer}>
            <TouchableOpacity style={styles.button} onPress={handleSalvar}>
              <Text style={styles.buttonText}>Salvar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={onClose}>
              <Text style={styles.buttonText}>Cancelar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(24, 24, 24, 0.6)',
  },
  scrollView: {
    flexGrow: 1,
    paddingTop: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    backgroundColor: '#FFF',
    width: '85%',
    paddingVertical: 24,
    paddingHorizontal: 16,
    borderRadius: 8,
  },
  title: {
    fontSize: 18,
    marginBottom: 16,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 4,
    marginBottom: 12,
    paddingHorizontal: 10,
  },
  button: {
    flex: 1,
    backgroundColor: '#007bff',
    paddingVertical: 12,
    borderRadius: 4,
    alignItems: 'center',
    marginVertical: 12,
    marginHorizontal: 5,
  },
  buttonText: {
    color: '#FFF',
    fontSize: 16,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  switchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  switchText: {
    marginRight: 10,
    fontSize: 16,
  },
});

export default ModalNovoMorador;

             
