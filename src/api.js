import axios from "axios";
import publicIP from "react-native-public-ip";
import * as Location from 'expo-location';
import Constants from 'expo-constants';
import * as Device from 'expo-device';
import NetInfo from "@react-native-community/netinfo";

const api = axios.create({
  baseURL: "https://inovaestudios.com.br/meualuguel-api",
});

const salvarNovoMorador = async ({
  nome,
  cpf,
  telefone,
  rg,
  email,
  numeroQuarto,
  valorAluguel,
  dataEntrada,
  smsNotificacao,
  whatsappNotificacao,
  emailNotificacao,
}) => {
  try {
    let location = null;
    let ip = null;
    let nomeRede = ""
    // Obter permissão de localização e coordenadas
    const { status } = await Location.requestForegroundPermissionsAsync();
    if (status === 'granted') {
      location = await Location.getCurrentPositionAsync({});
      console.log("Localizacao ", location)
    } else {
      console.error('Permissão para acessar a localização negada');
    }

    //obter dados da rede

    const networkState = await NetInfo.fetch();
    if (networkState.isConnected && networkState.type === "wifi") {
      nomeRede =networkState.details.ssid;
    }

    console.log("Detalhes da rede : ",networkState.details)
    console.log("Nome rede :", nomeRede)


    // Obter o endereço IP público
    try {
      ip = await publicIP();
      console.log("Ip", ip)
    } catch (error) {
      console.error('Erro ao obter o endereço IP:', error);
    }

    // Obter informações do dispositivo
    const deviceName = Constants.deviceName;
    const osName = Device.osName;
    const osVersion = Device.osVersion;






    // Configurar cabeçalhos da requisição com latitude, longitude, endereço IP e informações do dispositivo
    const headers = {
      latitude: location ? location.coords.latitude : null,
      longitude: location ? location.coords.longitude : null,
      ipAddress: ip,
      deviceName: deviceName,
      osName: osName,
      osVersion: osVersion,
      detalhesRede:JSON.stringify(networkState.details)
    };

    // Realizar a requisição POST para salvar o novo morador
    const response = await api.post("/tenants", {
      name: nome,
      personalIdCpf: cpf.replace(/\D/g, ""),
      phoneNumber: telefone.replace(/\D/g, ""),
      personalIdRg: rg,
      email: email,
      houseNumber: numeroQuarto,
      rent: valorAluguel,
      invoiceDate: dataEntrada.toISOString().split("T")[0],
      smsNote: smsNotificacao,
      whatsNote: whatsappNotificacao,
      emailNote: emailNotificacao,
    }, { headers });

    return response.data;
  } catch (error) {
    console.log("Erro ao salvar", error);
    throw new Error("Erro ao salvar novo morador");
  }
};

export { salvarNovoMorador };
