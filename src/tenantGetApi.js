

export async function tenantGetApi() {
    try {
      const response = await fetch('https://inovaestudios.com.br/meualuguel-api/tenants');
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error fetching data:', error);
      throw error; // Rethrow the error to be handled where the function is called
    }
  }
  