import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { Home } from "./pages/home";
import { Tenants } from "./pages/tenants";
import {Ionicons}  from "@expo/vector-icons"
import {MaterialIcons}  from "@expo/vector-icons"
import Payments from "./pages/payments";



const Tab = createBottomTabNavigator();

export function Routes() {
  
  return (
    <Tab.Navigator>
      <Tab.Screen
       name="home"
        component={Home} 
        options={{
            headerShown:false,
            tabBarIcon:({focused,size,color})=>{

                if(focused){
                        return <Ionicons size={size} color={color} name="home"/>
                }
                return <Ionicons size={size} color={color} name="home-outline"/>
            }
        }}
        />

      <Tab.Screen
       name="Moradores"
       component={Tenants} 
       options={{
        headerShown:false,
        tabBarIcon:({focused,size,color})=>{

            if(focused){
                    return <Ionicons size={size} color={color} name="list"/>
            }
            return <Ionicons size={size} color={color} name="list-outline"/>
        }
    }}
       />

<Tab.Screen
       name="Recebimentos"
        component={Payments} 
        options={{
            headerShown:false,
            tabBarIcon:({focused,size,color})=>{

                if(focused){
                        return <MaterialIcons name="attach-money" size={size} color={color} />
                }
                return <MaterialIcons name="attach-money" size={size} color={color} />
            }
        }}
        />
        
      
    </Tab.Navigator>
  );
}
